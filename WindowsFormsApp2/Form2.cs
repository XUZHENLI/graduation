﻿using MailKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sendAddress = this.textBox1.Text.ToString();
            string subject = this.textBox2.Text.ToString();
            string body = this.textBox3.Text.ToString();
            sendMail(sendAddress, subject, body);
            this.Close();
        }

        private void sendMail(string sendAddress, string subject, string body)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("", "zhenli.xu@tradex.com.sg"));
            message.To.Add(new MailboxAddress("", sendAddress));
            message.Subject = subject;
            message.Body = new TextPart("plain") { Text = body };

            using (var client = new SmtpClient(new ProtocolLogger("smtp.log")))
            {
                client.Connect("smtp.office365.com", 587, SecureSocketOptions.StartTls);

                client.Authenticate("zhenli.xu@tradex.com.sg", "Xzl112358");

                client.Send(message);
                client.Disconnect(true);
            }
        }
    }
}
