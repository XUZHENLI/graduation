﻿using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using OpenPop.Mime;
using OpenPop.Mime.Header;
using OpenPop.Pop3;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Resources;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public static string SearchKeyword;
        public static volatile int flag = 0;
        public static List<Email> commonMails = new List<Email>();
        public static List<string> mailUIDs = new List<string>();
        public int searchCount = 0;
        public int reCount = 0;
        //public delegate void DoDelegate(DataTable dataTable);

        public Form1()
        {
            InitializeComponent();

            TestClass testClass = new TestClass();
            TestClass.flag = flag;
            testClass.mainThread = new TestClass.TestDelegate(refreshAllEmails);

            Thread oneThread = new Thread(new ThreadStart(testClass.refreshFunction));
            oneThread.Start();
        }

        public void refreshAllEmails(DataTable dt)
        {
            if (this.gridControl1.InvokeRequired)
            {
                TestClass testClass = new TestClass();
                testClass.mainThread = new TestClass.TestDelegate(refreshAllEmails);
                this.Invoke(testClass.mainThread, new object[] { dt });
            }
            else
            {
                this.gridControl1.DataSource = null;
                this.gridControl1.DataSource = dt;
                this.gridView1.OptionsView.ShowGroupPanel = false;
                gridView1.Columns["Body"].Visible = false;
                gridView1.Columns["Uid"].Visible = false;

                foreach (DataRow dr in dt.Rows)
                {
                    string uid = (string)dr["Uid"];
                    if (!mailUIDs.Contains(uid))
                    {
                        Email email = new Email((string)dr["Sender"], (string)dr["Recipient"], (string)dr["Subject"], (string)dr["Body"], (string)dr["Uid"]);
                        mailUIDs.Add(uid);
                        commonMails.Add(email);
                    }
                }
            }
        }

        public void refreshFilterResult(DataTable dt)
        {
            if (this.gridControl1.InvokeRequired)
            {
                //DoDelegate v = new DoDelegate(refreshTable1);
                //this.Invoke(v, new object[] { dt });
                TestClass testClass = new TestClass();
                testClass.mainThread = new TestClass.TestDelegate(refreshFilterResult);
                this.Invoke(testClass.mainThread, new object[] { dt });
            }
            else
            {
                this.gridControl1.DataSource = null;
                this.gridControl1.DataSource = dt;
                this.gridView1.OptionsView.ShowGroupPanel = false;
                gridView1.Columns["Body"].Visible = false;
                gridView1.Columns["Uid"].Visible = false;
            }
        }

        private void gridControl1_MouseDown(object sender, MouseEventArgs e)
        {
            GridHitInfo hInfo = gridView1.CalcHitInfo(new Point(e.X, e.Y));
            if (e.Button == MouseButtons.Left && e.Clicks == 2)
            {
                if (hInfo.InRow)
                {
                    string rowInfo = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Body").ToString();
                    Form3 form3 = new Form3(rowInfo);
                    form3.ShowDialog();
                }
            }
        }

        private void radioButton2_CheckedChanged_1(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("");
            ResourceManager r = new ResourceManager("WindowsFormsApp2.form1", typeof(Form1).Assembly);
            button1.Text = r.GetString("button1.Text");
            button2.Text = r.GetString("button2.Text");
            button3.Text = r.GetString("button3.Text");
            Language.Text = r.GetString("Language.Text");
            radioButton1.Text = r.GetString("radioButton1.Text");
            radioButton2.Text = r.GetString("radioButton2.Text");
        }

        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-SG");
            ResourceManager r = new ResourceManager("WindowsFormsApp2.form1", typeof(Form1).Assembly);
            button1.Text = r.GetString("button1.Text");
            button2.Text = r.GetString("button2.Text");
            button3.Text = r.GetString("button3.Text");
            Language.Text = r.GetString("Language.Text");
            radioButton1.Text = r.GetString("radioButton1.Text");
            radioButton2.Text = r.GetString("radioButton2.Text");
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            flag = 0;
            textBox1.Text = null;
            this.button1_Click_1(null, null);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            SearchKeyword = textBox1.Text.ToString();
            if (SearchKeyword != null)
            {
                searchCount++;
                flag = 1;


                if (searchCount == 1)
                {
                    TestClass testClass = new TestClass();
                    TestClass.flag = 1;

                    testClass.mainThread = new TestClass.TestDelegate(refreshFilterResult);
                    Thread oneThread = new Thread(new ThreadStart(testClass.FilterFunction));
                    oneThread.Start();
                }
            }
        }
    }


    public class TestClass
    {
        public static volatile int flag = 0;
        public List<Email> mailbox = new List<Email>();

        HashSet<string> emailUids = new HashSet<string>();


        public delegate void TestDelegate(DataTable dataTable);
        public TestDelegate mainThread;

        private Pop3Client enterMail()
        {
            Pop3Client client = new Pop3Client();
            client.Connect("outlook.office365.com", 995, true);
            client.Authenticate("zhenli.xu@tradex.com.sg", "Xzl112358");
            return client;
        }

        private void getEmails()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);


            Pop3Client client = enterMail();
            int messageCount = client.GetMessageCount();

            for (int i = 1; i <= messageCount; i++)
            {
                string uid = client.GetMessageUid(i);
                if (!emailUids.Contains(uid))
                {
                    Email email = new Email();
                    OpenPop.Mime.Message message = client.GetMessage(i);
                    email.Sender = message.Headers.From.Address;

                    List<RfcMailAddress> recipients = message.Headers.To;
                    string recipient = "";
                    for (int j = 0; j < recipients.Count; j++)
                    {
                        recipient += recipients[j];

                    }
                    email.Recipient = recipient;

                    email.Subject = message.Headers.Subject;

                    MessagePart messagePart = message.MessagePart;
                    string body = "";
                    if (messagePart.IsText)
                    {
                        body = messagePart.GetBodyAsText();
                    }
                    else if (messagePart.IsMultiPart)
                    {
                        MessagePart plainTextPart = message.FindFirstPlainTextVersion();

                        if (plainTextPart != null)
                        {
                            body = plainTextPart.GetBodyAsText();
                        }
                        else
                        {
                            List<MessagePart> textVersions = message.FindAllTextVersions();
                            if (textVersions.Count >= 1)
                                body = textVersions[0].GetBodyAsText();
                            else
                                body = "Cannot find a text version body in this message.";
                        }
                    }
                    email.Body = body;
                    email.Uid = uid;
                    mailbox.Add(email);
                }
            }
        }

        private DataTable initializeDataTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Sender", typeof(string));
            dt.Columns.Add("Recipient", typeof(string));
            dt.Columns.Add("Subject", typeof(string));
            dt.Columns.Add("Body", typeof(string));
            dt.Columns.Add("Uid", typeof(string));
            return dt;
        }

        private DataTable prepareData()
        {
            DataTable dt = initializeDataTable();

            foreach (Email el in mailbox)
            {
                dt.Rows.Add(new string[] { el.Sender, el.Recipient, el.Subject, el.Body, el.Uid });
            }
            return dt;
        }

        private DataTable filter(List<Email> mls, string kw)
        {
            DataTable dtfil = initializeDataTable();

            foreach (Email el in mls)
            {
                if (el.Body == null)
                {
                    if (el.Sender.Contains(kw) || el.Recipient.Contains(kw) || el.Subject.Contains(kw))
                    {

                        dtfil.Rows.Add(new string[] { el.Sender, el.Recipient, el.Subject, el.Body, el.Uid });
                    }
                }
                else
                {
                    if (el.Sender.Contains(kw) || el.Recipient.Contains(kw) || el.Subject.Contains(kw) || el.Body.Contains(kw))
                    {

                        dtfil.Rows.Add(new string[] { el.Sender, el.Recipient, el.Subject, el.Body, el.Uid });
                    }
                }
            }

            return dtfil;
        }

        public void refreshFunction()
        {
            while (true)
            {
                while (flag == 0)
                {
                    getEmails();
                    mainThread(prepareData());
                    Thread.Sleep(10000);
                }
            }
        }

        public void FilterFunction()
        {
            while (true)
            {
                while (flag == 1)
                {
                    mainThread(filter(Form1.commonMails, Form1.SearchKeyword));
                    Thread.Sleep(10000);
                }
            }

        }
    }
}
