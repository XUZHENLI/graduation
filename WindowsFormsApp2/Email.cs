﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    public class Email
    {
        private string sender;
        private string recipient;
        private string subject;
        private string body;
        private string uid;
        public Email(string sender, string recipient, string subject, string body, string uid)
        {
            this.sender = sender;
            this.recipient = recipient;
            this.subject = subject;
            this.body = body;
            this.uid = uid;
        }
        public Email()
        {

        }
        public string Sender { get => sender; set => sender = value; }
        public string Recipient { get => recipient; set => recipient = value; }
        public string Subject { get => subject; set => subject = value; }
        public string Body { get => body; set => body = value; }
        public string Uid { get => uid; set => uid = value; }

    }
}
